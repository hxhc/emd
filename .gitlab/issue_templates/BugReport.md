## Summary

(Summarize the bug encountered concisely)

## Versions and context

(Which EMD/Python versions (eg EMD v0.5.2 on Python 3.9) and which operating system (eg Debian 11.2, Mac OSX 10.15 or Windows 10) are you using?)

## Steps to reproduce

(How one can reproduce the issue - this is very important. Please use (`) to format inline code and (```) to format code blocks)

## What is the current bug behavior?

(What happens at the moment and why is this wrong?)

## What is the expected correct behavior?

(What should happen instead)

## Relevant logs and/or screenshots

(Paste any relevant logs - please use code blocks (```) to format console output, logs, and code, as
it's very hard to read otherwise.)

## Possible fixes

(If you can, link to the line of code that might be responsible for the problem, any suggestions for fixes are welcome!)
